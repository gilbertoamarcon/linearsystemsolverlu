#include "sistemaLinear.hpp"

SistemaLinear::SistemaLinear(){}

// ----------------------------------------------------------------
// Método público para solução de SL Ax=b
// Entradas: A, x, b, ordem (ordem de A),
// ----------------------------------------------------------------
void SistemaLinear::resolve(float** A,float* x,float* b,int ordem){
    o = ordem;
    L = inicializaMatriz(o,o);
    U = inicializaMatriz(o,o);
    M = inicializaMatriz(o,o+1); 
    preparaMatriz(A,b);
    fatoracaoLU();
    for(int i = 0; i < o; i++)
        x[i] = M[i][o];
    preparaMatriz(L,x);
    retrosubstituicaoL(x);
    preparaMatriz(U,x);
    retrosubstituicaoU(x);
    free(L);
    free(U);
    free(M);
}

// Aloca espaco de memória para a matriz
float** SistemaLinear::inicializaMatriz(int m,int n){
    float** M;
    M = (float**)calloc(m, sizeof(float*));
    for(int i = 0; i < m; i++){
    	M[i] = (float*)calloc(n, sizeof(float));
        for(int j = 0; j < n; j++)
            M[i][j] = 0;
    }
    return M;
}

// Copia o conteúdo da matriz A e do vetor b para a matriz M
void SistemaLinear::preparaMatriz(float** H,float* v){
    for (int i = 0; i < o; i++){
        for (int j = 0; j < o; j++)
            M[i][j] = H[i][j];
        M[i][o] = v[i];
    }
}

void SistemaLinear::retrosubstituicaoU(float* x){

    // Este loop Sobe a matriz escalonada achando o valor de cada variável
    for (int i = o-1; i >= 0; i--){

        // Preenche o vetor x com b
        x[i] = M[i][o];

        // Subtrai de x todos os valores ponderados entre b e o pivô
        for (int j = (o-1); j > i; j--)
            x[i] -= M[i][j]*x[j];

        // Divide x pelo pivô
        x[i] /= M[i][i];
    }

}

void SistemaLinear::retrosubstituicaoL(float* x){

    // Este loop Sobe a matriz escalonada achando o valor de cada variável
    for (int i = 0; i < o; i++){

        // Preenche o vetor x com b
        x[i] = M[i][o];

        // Subtrai de x todos os valores ponderados entre b e o pivô
        for (int j = 0; j < i; j++)
            x[i] -= M[i][j]*x[j];

        // Divide x pelo pivô
        x[i] /= M[i][i];
    }

}

void SistemaLinear::fatoracaoLU(){

    // Zera os elementos de L e U triangularizando-as. Preenche com 1 os elementos da diagonal de L
    for (int i = 0; i < o; i++){
        for (int j = 0; j < o; j++){
            if( i > j)
                U[i][j] = 0;
            else if(i < j)
                L[i][j] = 0;
            else
                L[i][j] = 1;
        }
    }

    // Checagem e permutação de linhas
    for (int i = 0; i < o; i++){

        // Detecta pivot maximo
        int maxPivot = i;
        for(int j = i+1; j < o; j++)
            if(abs(M[j][i]) > abs(M[i][i]))
                maxPivot = j;

        // Troca linhas
        if(maxPivot != i){
            for(int j = 0; j <= o; j++){
                float flaux     = M[i][j];
                M[i][j]         = M[maxPivot][j];
                M[maxPivot][j]  = flaux;
            }
        }  
    }

    float soma = 0; // Variável auxiliar que armazena os valores a serem subtraídos a cada passo
    
    for (int i = 0; i < o; i++){

        // A cada passada deste laço, define-se uma linha de U
        for (int j = i; j < o; j++){
            soma = 0;
            for (int k = 0; k < i; k++)
                soma += L[i][k]*U[k][j];
            U[i][j] = M[i][j] - soma;
        }

        // A cada passada deste laço, define-se uma coluna de L
        for (int j = i+1; j < o; j++){
            soma = 0;
            for (int k = 0; k < j; k++)
                soma += L[j][k]*U[k][i];
            L[j][i] = (M[j][i] - soma)/U[i][i];
        }

    }
}
