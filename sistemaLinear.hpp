#ifndef __SISTEMALINEAR_HPP__
#define __SISTEMALINEAR_HPP__
#include <stdlib.h>

class SistemaLinear{
    private:
        int o;
        float** L;
        float** U;
        float** M;
        float** inicializaMatriz(int m,int n);
        void preparaMatriz(float** H,float* v);
        void retrosubstituicaoU(float* x);
        void retrosubstituicaoL(float* x);
        void fatoracaoLU();
    public:
        SistemaLinear();
        void resolve(float** A,float* x,float* b,int ordem);
};

#endif