# README #

This is a lightweight linear (Ax=b) C++ system solver that uses no external libraries. 

The solver is based on LU factorization and memory allocation is done dynamically.

All the necessary code for solving is embedded in a separate class. 