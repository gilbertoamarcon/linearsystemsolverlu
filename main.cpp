#include "sistemaLinear.hpp"
#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main(int argc, char **argv){

	int ordem = 4;
	SistemaLinear sistemaLinear;

    float** A;
    A = (float**)calloc(ordem, sizeof(float*));
    for( int i = 0; i < ordem; i++)
    	A[i] = (float*)calloc(ordem, sizeof(float));

    float* x;
    x = (float*)calloc(ordem,sizeof(float));

    float* b;
    b = (float*)calloc(ordem,sizeof(float));

    A[0][0] = 0;
    A[0][1] = 0;
    A[0][2] = 0;
    A[0][3] = 1;

    A[1][0] = 1;
    A[1][1] = 0;
    A[1][2] = 0;
    A[1][3] = 0;

    A[2][0] = 0;
    A[2][1] = 1;
    A[2][2] = 0;
    A[2][3] = 0;

    A[3][0] = 0;
    A[3][1] = 0;
    A[3][2] = 1;
    A[3][3] = 0;

    b[0] = 3;
    b[1] = 7;
    b[2] = 8;
    b[3] = 9;

	sistemaLinear.resolve(A,x,b,ordem);

	cout << setprecision(6) << setw(12) << x[0] << endl;
    cout << setprecision(6) << setw(12) << x[1] << endl;
    cout << setprecision(6) << setw(12) << x[2] << endl;
	cout << setprecision(6) << setw(12) << x[3] << endl;

	return 0;
}